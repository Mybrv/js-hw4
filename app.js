/* 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні, то запитуйте у
користувача нове занчення до тих пір, поки воно не буде числом. Виведіть на екран всі числа від меншого до більшого за
допомогою циклу for. */

let firstNumber = +prompt("Enter first number")
let secondNumber = +prompt("Enter second number")

while (isNaN(firstNumber) || isNaN(secondNumber)) {
    if (isNaN(firstNumber)) {
        firstNumber = +prompt("Not a number. Enter first number")
    } else  {
        secondNumber = +prompt("Not a number. Enter second number")
    }
}
if (firstNumber > secondNumber) {
    for(i = secondNumber; i < firstNumber; i++) {
    console.log(i)
    }
} else if (secondNumber > firstNumber) {
    for (i = firstNumber; i < secondNumber; i++) {
        console.log(i)
    }
} else {
    console.log("Numbers are equal")
}
/* 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. Якщо введене значення не є
парним числом, то запитуйте число доки користувач не введе правильне значення. */

let userNumber = +prompt("Enter number")

while (userNumber % 2 !== 0) {
    userNumber = +prompt("Odd number. Enter an even number")
}